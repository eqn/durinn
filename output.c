#include <sys/socket.h>

#include <errno.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "durinn.h"

extern char *nick;
extern char *prefix;
extern char *joinprefix;
extern char *partprefix;
extern char *quitprefix;

enum {
	CTCP_ACTION,
	CTCP_VERSION,
	CTCP_UNKNOWN
};

int ctcptype(char *);
void ctcp_reply(int, char *);

char *
doprefix(Msg *m)
{
	switch (m->type.id) {
	case CMD_PRIVMSG:
	case CMD_NOTICE:
		return NULL;
	case CMD_JOIN:
		return joinprefix;
	case CMD_PART:
		return partprefix;
	case CMD_QUIT:
		return quitprefix;
	default:
		return prefix;
	}
}

char *
formatout(Msg *m)
{
	char buf[MAXMSGLEN];
	char *tmp;
	char *mnick;
	char *mnickhost;
	char *time;

	memset(buf, 0, sizeof(buf));
	mnick = msgnick(m);
	mnickhost = msgnickhost(m);

	switch (m->type.id) {
	case CMD_NICK:
		if (!strcmp(mnick, nick)) {
			snprintf(buf, sizeof(buf), "You are now known as %s",
			    m->params[0]);
			nick = strdup(m->params[0]);
		} else
			snprintf(buf, sizeof(buf), "%s is now known as %s",
			    mnick, m->params[0]);
		break;
	case CMD_MODE:
		if (!strcmp(m->prefix, nick))
			snprintf(buf, sizeof(buf), "mode %s for user %s",
			    m->params[1], m->params[0]);
		else {
			tmp = trim(paramsfrom(m->params, 1));
			snprintf(buf, sizeof(buf), "mode %s [%s] by %s",
			    m->params[0], tmp,
			    (mnick == NULL) ? m->prefix : mnick);
			free(tmp);
		}
		break;
	case CMD_QUIT:
		if (m->params[0] == NULL || *m->params[0] == '\0')
			snprintf(buf, sizeof(buf), "%s (%s) quit", mnick,
			    mnickhost);
		else
			snprintf(buf, sizeof(buf), "%s (%s) quit (%s)",
			    mnick, mnickhost, m->params[0]);
		break;
	case CMD_JOIN:
		if (!strcmp(mnick, nick))
			add_channel(m->params[0]);
		snprintf(buf, sizeof(buf), "%s (%s) joined to %s", mnick,
		    mnickhost, m->params[0]);
		break;
	case CMD_PART:
		if (!strcmp(mnick, nick))
			remove_channel(m->params[0]);
		if (m->params[1] == NULL)
			snprintf(buf, sizeof(buf), "%s (%s) left from %s",
			    mnick, mnickhost, m->params[0]);
		else
			snprintf(buf, sizeof(buf), "%s (%s) left from %s (%s)",
			    mnick, mnickhost, m->params[0], m->params[1]);
		break;
	case CMD_TOPIC:
		snprintf(buf, sizeof(buf), "%s changed the topic of %s to: %s",
		    mnick, m->params[0], m->params[1]);
		break;
	case CMD_PRIVMSG:
		/* TODO: add replying to CTCP queries */
		if (*(m->params[1]) == '\001') { /* CTCP */
			switch (ctcptype(m->params[1] + 1)) {
			case CTCP_ACTION:
				snprintf(buf, sizeof(buf), "%s: * %s %s",
				    m->params[0], mnick, m->params[1] + 8);
				break;
			case CTCP_VERSION:
				snprintf(buf, sizeof(buf),
				    "-- %s requested CTCP VERSION from %s",
				    mnick, m->params[0]);
				ctcp_reply(CTCP_VERSION, mnick);
				break;
			default:
				snprintf(buf, sizeof(buf), "-- %s: %s", mnick,
				    m->params[1]);
				break;
			}
		} else {
			snprintf(buf, sizeof(buf), "%s: <%s> %s", m->params[0],
			    mnick, m->params[1]);
		}
		break;
	case CMD_NOTICE:
		if (!strcmp(m->params[0], nick) || !strcmp(m->params[0], "*"))
			snprintf(buf, sizeof(buf), "-%s- %s", m->prefix,
			    m->params[1]);
		else
			snprintf(buf, sizeof(buf), "%s: -%s- %s", m->params[0],
			    m->prefix, m->params[1]);
		break;
	case CMD_PING:
		pong(m->params[0]);
		free(mnick);
		free(mnickhost);
		return NULL;
	case CMD_ERROR:
		wprintf("quitting %s: %s\n", argv0, m->params[0]);
		freemsg(m);
		free(mnick);
		free(mnickhost);
		exit(1);
	case RPL_WELCOME:
	case RPL_YOURHOST:
	case RPL_CREATED:
	case RPL_MOTDSTART:
	case RPL_MOTD:
	case RPL_ENDOFMOTD:
	case RPL_UNAWAY:
	case RPL_NOWAWAY:
	case RPL_LUSERCLIENT:
	case RPL_LUSERME:
	case ERR_NONICKNAMEGIVEN:
		snprintf(buf, sizeof(buf), "%s", m->params[1]);
		break;
	case RPL_WHOISUSER:
		snprintf(buf, sizeof(buf), "%s: (%s@%s): %s", m->params[1],
		    m->params[2], m->params[3], m->params[5]);
		break;
	case RPL_WHOISCHANNELS:
	case RPL_ENDOFWHOIS:
	case RPL_ENDOFNAMES:
	case ERR_NOSUCHNICK:
	case ERR_NOSUCHCHANNEL:
	case ERR_ERRONEUSNICKNAME:
	case ERR_NICKNAMEINUSE:
	case ERR_NICKCOLLISION:
	case ERR_UNAVAILRESOURCE:
		snprintf(buf, sizeof(buf), "%s: %s", m->params[1],
		    m->params[2]);
		break;
	case RPL_WHOISSERVER:
		snprintf(buf, sizeof(buf), "%s: %s (%s)", m->params[1],
		    m->params[2], m->params[3]);
		break;
	case RPL_AWAY:
		snprintf(buf, sizeof(buf), "%s: is away (%s)", m->params[1],
		    m->params[2]);
		break;
	case RPL_NAMREPLY:
		snprintf(buf, sizeof(buf), "%s: %s", m->params[2],
		    m->params[3]);
		break;
	case RPL_LUSEROP:
	case RPL_LUSERUNKNOWN:
	case RPL_LUSERCHANNELS:
		snprintf(buf, sizeof(buf), "%s %s", m->params[1], m->params[2]);
		break;
	case RPL_TOPIC:
		if (m->params[2] == NULL || *m->params[2] == '\0')
			break;
		snprintf(buf, sizeof(buf), "topic for %s: %s", m->params[1],
		    m->params[2]);
		break;
	case RPL_WHOISIDLE:
		time = fmttimes(m->params[3], "%Y-%m-%d %H:%M:%S");
		snprintf(buf, sizeof(buf), "%s: %s %s %s", m->params[1],
		    m->params[2], m->params[4], time);
		free(time);
		break;
	case RPL_CHANNELMODEIS:
		snprintf(buf, sizeof(buf), "mode for %s is %s", m->params[1],
		    m->params[2]);
		break;
	case NONE:
		/*
		 * Handle replies not conforming to RFC 2812, which are used
		 * by for example freenode.
		 *
		 * Numeric replies: 328, 329, 330, 333, 671
		 */

		/* freenode: homepage for channel */
		if (!strcmp(m->type.raw, "328"))
			snprintf(buf, sizeof(buf), "Homepage for %s: %s",
			    m->params[1], m->params[2]);
		/* freenode: channel creation time */
		else if (!strcmp(m->type.raw, "329")) {
			time = fmttimes(m->params[2], "%Y-%m-%d %H:%M:%S");
			snprintf(buf, sizeof(buf), "Channel %s created %s",
			    m->params[1], time);
			free(time);
		}
		/* freenode: is logged in as */
		else if (!strcmp(m->type.raw, "330"))
			snprintf(buf, sizeof(buf), "%s: %s %s", m->params[1],
			    m->params[3], m->params[2]);
		/* freenode: RPL_TOPICWHOTIME */
		else if (!strcmp(m->type.raw, "333")) {
			time = fmttimes(m->params[3], "%Y-%m-%d %H:%M");
			snprintf(buf, sizeof(buf), "%s: topic set by %s at %s",
			    m->params[1], m->params[2], time);
			free(time);
		}
		/* freenode: is using a secure connection */
		else if (!strcmp(m->type.raw, "671"))
			snprintf(buf, sizeof(buf), "%s: %s", m->params[1],
			    m->params[2]);
		else {
			tmp = paramsfrom(m->params, 1);
			snprintf(buf, sizeof(buf), "%s %s", m->type.raw, tmp);
			free(tmp);
		}
		break;
	default:
		tmp = paramsfrom(m->params, 1);
		snprintf(buf, sizeof(buf), "%s %s", m->type.raw, tmp);
		free(tmp);
	}

	free(mnick);
	free(mnickhost);

	return strdup(buf);
}

int
isctcp(Msg *m)
{
	if (*(m->params[1]) == '\001')
		return 1;
	return 0;
}

int
ctcptype(char *params)
{
	if (!strncmp(params, "ACTION", 6))
		return CTCP_ACTION;
	else if (!strncmp(params, "VERSION", 7))
		return CTCP_VERSION;
	return CTCP_UNKNOWN;
}

void
printownline(char *s)
{
	char *time;

	time = maketime("%H:%M");
	s[strlen(s) - 2] = '\0';
	printf("%s %s: <%s> %s\n", time, target, nick, strchr(s, ':') + 1);
	free(time);
}

void
printownaction(char *s)
{
	char *time;

	time = maketime("%H:%M");
	s[strlen(s) - 2] = '\0';
	printf("%s %s: * %s %s\n", time, target, nick, strchr(s, ':') + 9);
	free(time);
}

void
ctcp_reply(int type, char *target)
{
	char buf[MAXMSGLEN];

	switch (type) {
	case CTCP_VERSION:
		snprintf(buf, sizeof(buf), "NOTICE %s :%cVERSION %s-%s%c\r\n",
		    target, '\001', basename(argv0), VERSION, '\001');
		break;
	default:
		; /* do nothing yet */
	}
	if ((send(s, buf, strlen(buf), 0)) < 0)
		wprintf("send: %s\n", strerror(errno));
}
