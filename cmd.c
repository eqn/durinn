#include <sys/socket.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "arg.h"
#include "durinn.h"

typedef struct Cmdtab Cmdtab;
struct Cmdtab {
	char cmdc;
	int t;		/* takes a target? */
	int n;		/* takes a nick? */
	char *(*fn)(Cmd *);
} cmdtab[];

extern char *nick;
extern char *realname;

int	 lookup(char);
char	*next_cmdarg(char **);
char	*A_cmd(Cmd *);
char	*M_cmd(Cmd *);
char	*N_cmd(Cmd *);
char	*a_cmd(Cmd *);
char	*c_cmd(Cmd *);
char	*i_cmd(Cmd *);
char	*j_cmd(Cmd *);
char	*l_cmd(Cmd *);
char	*m_cmd(Cmd *);
char	*n_cmd(Cmd *);
char	*p_cmd(Cmd *);
char	*q_cmd(Cmd *);
char	*r_cmd(Cmd *);
char	*t_cmd(Cmd *);
char	*w_cmd(Cmd *);

Cmdtab cmdtab[] = {
/*	cmdc,	t,	n,	fn	*/
	{'A',	1,	0,	A_cmd},
	{'M',	1,	0,	M_cmd},
	{'N',	1,	0,	N_cmd},
	{'a',	0,	0,	a_cmd},
	{'c',	1,	0,	c_cmd},
	{'i',	0,	0,	i_cmd},
	{'j',	1,	0,	j_cmd},
	{'l',	0,	0,	l_cmd},
	{'m',	1,	0,	m_cmd},
	{'n',	0,	1,	n_cmd},
	{'p',	1,	0,	p_cmd},
	{'q',	0,	0,	q_cmd},
	{'r',	0,	0,	r_cmd},
	{'t',	1,	0,	t_cmd},
	{'w',	0,	1,	w_cmd},
	{0,	0,	0,	0}
};

int
cmdexec(Cmd *cmd)
{
	int i;
	char *res;

	if ((i = lookup(cmd->cmdc)) < 0) {
		wprintf("%c: unrecognized command\n", cmd->cmdc);
		return 0;
	}
	if ((res = (cmdtab[i].fn)(cmd)) == NULL)
		return 0;
	if (send(s, res, strlen(res), 0) < 0) {
		wprintf("send: %s\n", strerror(errno));
		return 0;
	}
	if (cmd->cmdc == 'm')
		printownline(res);
	else if (cmd->cmdc == 'A')
		printownaction(res);
	free(res);
	return 1;
}

Cmd *
createcmd(char cmdc, char *args)
{
	int i;
	Cmd *cmd;

	cmd = calloc(1, sizeof(*cmd));
	cmd->cmdc = cmdc;
	if ((i = lookup(cmdc)) < 0) {
		wprintf("%c: unrecognized command\n", cmdc);
		return NULL;
	}
	if (cmdtab[i].t)
		cmd->ctarget = next_cmdarg(&args);
	if (cmdtab[i].n)
		cmd->cnick = next_cmdarg(&args);
	cmd->args = args;

	return cmd;
}

int
lookup(char cmdc)
{
	int i;

	for (i = 0; cmdtab[i].cmdc; i++)
		if (cmdtab[i].cmdc == cmdc)
			return i;
	return -1;
}

char *
next_cmdarg(char **args)
{
	char *a;

	a = NULL;
	while ((a = strsep(args, " \t")) != NULL)
		if (*a != 0)
			break;
	return a;
}

char *
A_cmd(Cmd *c)
{
	char s[MAXMSGLEN];
	int ret;

	if (c->ctarget == NULL) {
		wprintf("no target\n");
		return NULL;
	}
	if (c->args == NULL) {
		wprintf("no action to send\n");
		return NULL;
	}
	ret = snprintf(s, sizeof(s), "PRIVMSG %s :%cACTION %s%c\r\n",
	    c->ctarget, '\001', c->args, '\001');
	if (ret == -1 || ret >= sizeof(s))
		return NULL;
	return strdup(s);
}

char *
M_cmd(Cmd *c)
{
	char s[MAXMSGLEN];
	int ret;

	if (c->ctarget == NULL) {
		wprintf("no target specified\n");
		return NULL;
	}
	if (c->args == NULL)
		ret = snprintf(s, sizeof(s), "MODE %s\r\n", c->ctarget);
	else
		ret = snprintf(s, sizeof(s), "MODE %s %s\r\n", c->ctarget,
		    c->args);
	if (ret == -1 || ret >= sizeof(s))
		return NULL;
	return strdup(s);
}

char *
N_cmd(Cmd *c)
{
	char s[MAXMSGLEN];
	int ret;

	if (c->ctarget == NULL) {
		wprintf("no target specified\n");
		return NULL;
	}
	ret = snprintf(s, sizeof(s), "NAMES %s\r\n", c->ctarget);
	if (ret == -1 || ret >= sizeof(s))
		return NULL;
	return strdup(s);
}

char *
a_cmd(Cmd *c)
{
	char s[MAXMSGLEN];
	int ret;

	if (c->args == NULL)
		ret = snprintf(s, sizeof(s), "AWAY\r\n");
	else
		ret = snprintf(s, sizeof(s), "AWAY :%s\r\n", c->args);
	if (ret == -1 || ret >= sizeof(s))
		return NULL;
	return strdup(s);
}

char *
c_cmd(Cmd *c)
{
	char s[MAXMSGLEN];
	int ret;

	if (c->ctarget == NULL) {
		wprintf("no target\n");
		return NULL;
	}
	if (c->args == NULL) {
		wprintf("not enough arguments\n");
		return NULL;
	}
	ret = snprintf(s, sizeof(s), "PRIVMSG %s :%c%s%c\r\n", c->ctarget,
	    '\001', c->args, '\001');
	if (ret == -1 || ret >= sizeof(s))
		return NULL;
	return strdup(s);
}

char *
i_cmd(Cmd *c)
{
	USED(c);
	printf("%s %s\n", nick, realname);
	return NULL;
}

char *
j_cmd(Cmd *c)
{
	char s[MAXMSGLEN];
	int ret;

	if (c->ctarget == NULL) {
		wprintf("no channel to join\n");
		return NULL;
	}
	ret = snprintf(s, sizeof(s), "JOIN %s\r\n", c->ctarget);
	if (ret == -1 || ret >= sizeof(s))
		return NULL;
	target = strdup(lasttarget(c->ctarget));
	return strdup(s);
}

char *
l_cmd(Cmd *c)
{
	Channel *ch;

	USED(c);
	if (LIST_EMPTY(&channelhead)) {
		printf("not joined to any channel\n");
		return NULL;
	}

	LIST_FOREACH(ch, &channelhead, channel_entry)
		printf("%s%s", ch->name,
		    LIST_NEXT(ch, channel_entry) ? "," : "\n");
	return NULL;
}

char *
m_cmd(Cmd *c)
{
	char s[MAXMSGLEN];
	int ret;

	if (c->ctarget == NULL) {
		wprintf("no target specified\n");
		return NULL;
	}
	target = strdup(c->ctarget);
	if (c->args == NULL) {
		wprintf("no message to send\n");
		return NULL;
	}
	ret = snprintf(s, sizeof(s), "PRIVMSG %s :%s\r\n", c->ctarget, c->args);
	if (ret == -1 || ret >= sizeof(s))
		return NULL;
	return strdup(s);
}

char *
n_cmd(Cmd *c)
{
	char s[MAXMSGLEN];
	int ret;

	if (c->cnick == NULL) {
		wprintf("no nick specified\n");
		return NULL;
	}
	ret = snprintf(s, sizeof(s), "NICK :%s\r\n", c->cnick);
	if (ret == -1 || ret >= sizeof(s))
		return NULL;
	return strdup(s);
}

char *
p_cmd(Cmd *c)
{
	char s[MAXMSGLEN];
	int ret;

	if (c->ctarget == NULL) {
		wprintf("no channel to leave\n");
		return NULL;
	}
	if (c->args == NULL)
		ret = snprintf(s, sizeof(s), "PART %s\r\n", c->ctarget);
	else
		ret = snprintf(s, sizeof(s), "PART %s :%s\r\n", c->ctarget,
		    c->args);
	if (ret == -1 || ret >= sizeof(s))
		return NULL;
	if (!strcmp(target, c->ctarget))
		target = NULL;
	return strdup(s);
}

char *
q_cmd(Cmd *c)
{
	char s[MAXMSGLEN];
	int ret;

	if (c->args == NULL)
		ret = snprintf(s, sizeof(s), "QUIT :leaving\r\n");
	else
		ret = snprintf(s, sizeof(s), "QUIT :%s\r\n", c->args);
	if (ret == -1 || ret >= sizeof(s))
		return NULL;
	return strdup(s);
}

char *
r_cmd(Cmd *c)
{
	char s[MAXMSGLEN];
	int ret;

	if (c->args == NULL) {
		wprintf("no command to send\n");
		return NULL;
	}
	ret = snprintf(s, sizeof(s), "%s\r\n", c->args);
	if (ret == -1 || ret >= sizeof(s))
		return NULL;
	return strdup(s);
}

char *
t_cmd(Cmd *c)
{
	if (c->ctarget == NULL) {
		if (target == NULL || *target == '\0')
			printf("no target\n");
		else
			printf("%s\n", target);
		return NULL;
	}
	if (strspace(c->ctarget) > -1) {
		wprintf("too many targets\n");
		return NULL;
	}
	target = strdup(c->ctarget);
	return NULL;
}

char *
w_cmd(Cmd *c)
{
	char s[MAXMSGLEN];
	int ret;

	if (c->cnick == NULL) {
		wprintf("no nick to check\n");
		return NULL;
	}
	ret = snprintf(s, sizeof(s), "WHOIS %s\r\n", c->cnick);
	if (ret == -1 || ret >= sizeof(s))
		return NULL;
	return strdup(s);
}
