#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <errno.h>
#include <netdb.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "arg.h"
#include "config.h"
#include "durinn.h"

int s;
char *target;
char *argv0;

static char *readline(char *);

static void
auth(void)
{
	char authmsg[MAXMSGLEN];
	int ret;

	memset(authmsg, 0, sizeof(authmsg));
	ret = snprintf(authmsg, sizeof(authmsg),
	    "USER %s 8 * :%s\r\nNICK %s\r\n", nick, realname, nick);
	if (ret == -1 || ret >= sizeof(authmsg)) {
		wprintf("authmsg was truncated\n");
		return;
	}
	if (send(s, authmsg, strlen(authmsg), 0) < 0)
		eprintf("auth: send failed: %s\n", strerror(errno));
}

static void
handle_input(void)
{
	char buf[MAXMSGLEN];
	char msg[MAXMSGLEN];
	ssize_t n;
	int ret;
	char *params;
	Cmd *cmd;

	memset(buf, 0, sizeof(buf));
	memset(msg, 0, sizeof(msg));

	if ((n = read(0, buf, sizeof(buf))) < 0) {
		wprintf("read: %s\n", strerror(errno));
		return;
	}
	if (n == 0) {
		wprintf("quitting %s\n", argv0);
		exit(0);
	}
	buf[n - 1] = '\0'; /* Remove trailing newline */
	if (buf == NULL || *buf == '\0')
		return;

	if (buf[0] == '/') {
		if (buf[1] == '\0') {
			wprintf("no command specified\n");
			return;
		}
		if (!(buf[2] == '\0' || buf[2] == ' ')) {
			wprintf("invalid command\n");
			return;
		}
		if ((params = strchr(buf, ' ')) != NULL)
			params++;

		cmd = createcmd(buf[1], params);
		cmdexec(cmd);
		free(cmd);
		return;
	}

	if (target == NULL || *target == '\0') {
		wprintf("no target\n");
		return;
	}
	ret = snprintf(msg, sizeof(msg), "%s %s", target, buf);
	if (ret == -1 || ret >= sizeof(msg)) {
		wprintf("msg was truncated\n");
		return;
	}
	cmd = createcmd('m', msg);
	cmdexec(cmd);
	free(cmd);
}

static void
handle_server_output(void)
{
	static char buf[MAXRCVLEN + 1];
	size_t len;
	ssize_t n;
	Msg *m;
	char *line;
	char *prfx;
	char *res;
	char *time;

	len = strlen(buf);
	if ((n = recv(s, &buf[len], sizeof(buf) - len, 0)) < 0) {
		wprintf("recv: %s\n", strerror(errno));
		return;
	}
	buf[len + n] = '\0';

	while ((line = readline(buf)) != NULL) {
		m = calloc(1, sizeof(*m));
		prfx = NULL;
		parsemsg(line, m);
		res = formatout(m);

		if (res == NULL || *res == '\0') {
			freemsg(m);
			free(line);
			free(res);
			continue;
		}

		prfx = doprefix(m);
		time = maketime("%H:%M");
		if (prfx == NULL)
			printf("%s %s\n", time, res);
		else
			printf("%s %s %s\n", time, prfx, res);
		freemsg(m);
		free(line);
		free(res);
		free(time);
	}
}

static int
initconn(void)
{
	int s;
	struct addrinfo *res, *res0, hints;
	int error;
	int save_errno;
	const char *cause = NULL;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = PF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	error = getaddrinfo(host, port, &hints, &res0);
	if (error)
		eprintf("%s\n", gai_strerror(error));
	s = -1;
	for (res = res0; res; res = res->ai_next) {
		s = socket(res->ai_family, res->ai_socktype,
		    res->ai_protocol);
		if (s == -1) {
			cause = "socket";
			continue;
		}

		if (connect(s, res->ai_addr, res->ai_addrlen) == -1) {
			cause = "connect";
			save_errno = errno;
			close(s);
			errno = save_errno;
			s = -1;
			continue;
		}

		break;
	}
	if (s == -1)
		eprintf("%s\n", cause);
	freeaddrinfo(res0);

	return s;
}

static char *
readline(char *buf)
{
	char line[MAXMSGLEN];
	char *bufp = buf;
	int crlf;

	memset(line, 0, sizeof(line));
	if ((crlf = memcrlf(bufp, strlen(buf))) > 0 && crlf < MAXMSGLEN) {
		bufp[crlf] = '\0';
		strlcpy(line, bufp, crlf + 1);
		bufp += crlf + 2;
		memmove(buf, bufp, MAXRCVLEN + 3 + crlf - strlen(buf));
		bufp = buf;
		bufp[strlen(buf)] = '\0';
		return strdup(line);
	}
	return NULL;
}

static void
run(void)
{
	struct timeval timeout;
	fd_set readfds;
	int ready;

	for (;;) {
		FD_ZERO(&readfds);
		FD_SET(0, &readfds);
		FD_SET(s, &readfds);

		timeout.tv_sec = 120;
		timeout.tv_usec = 0;

		ready = select(s + 1, &readfds, 0, 0, &timeout);
		if (ready < 0) {
			if (errno == EINTR)
				continue;
			eprintf("select: %s\n", strerror(errno));
		} else if (!ready) {
			continue;
		}
		if (FD_ISSET(s, &readfds))
			handle_server_output();
		if (FD_ISSET(0, &readfds))
			handle_input();
	}
}

static void
usage(void)
{
	eprintf("usage: %s [-h host] [-n nick] [-p port] [-r realname]"
	    " [-t target] [-v]\n", argv0);
}

int
main(int argc, char *argv[])
{
	int tflag;
	struct passwd *pwd;
	char *t;

	if ((pwd = getpwuid(getuid())) == NULL)
		eprintf("pwd: %s\n", strerror(errno));

	argv0 = argv[0];
	tflag = 0;
	if (nick == NULL || *nick == '\0')
		nick = strdup(pwd->pw_name);

	ARGBEGIN {
	case 'h':
		host = strdup(EARGF(usage));
		break;
	case 'n':
		nick = strdup(EARGF(usage));
		break;
	case 'p':
		port = strdup(EARGF(usage));
		break;
	case 'r':
		realname = strdup(EARGF(usage));
		break;
	case 't':
		t = EARGF(usage);
		tflag = 1;
		break;
	case 'v':
		printf("%s-%s\n", argv0, VERSION);
		return 0;
	default:
		usage();
	} ARGEND;

	LIST_INIT(&channelhead);
	s = initconn();
	auth();
	if (tflag)
		join(t);
	run();
	close(s);

	return 0;
}
