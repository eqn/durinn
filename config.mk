PREFIX = /usr/local
MANPREFIX = $(PREFIX)/man

CPPFLAGS = -DVERSION=\"${VERSION}\"
CFLAGS = -g -Wall -Wextra -Winline -std=c99 -pedantic
