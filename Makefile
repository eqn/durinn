VERSION = 0.0

include config.mk

OBJ =\
	channel.o\
	cmd.o\
	durinn.o\
	msg.o\
	output.o\
	strlcat.o\
	strlcpy.o\
	strtonum.o\
	util.o

BIN = durinn

all: $(BIN)

$(BIN): $(OBJ)
	$(CC) $(CFLAGS) -o $@ $(OBJ) $(LDLIBS)

channel.o: durinn.h
cmd.o: durinn.h
durinn.o: arg.h config.h durinn.h
msg.o: durinn.h
output.o: durinn.h
strlcat.o: durinn.h
strlcpy.o: durinn.h
strtonum.o: durinn.h
util.o: durinn.h
durinn.h: irc.h queue.h

install: all
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f $(BIN) $(DESTDIR)$(PREFIX)/bin
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	cp -f $(BIN).1 $(DESTDIR)$(MANPREFIX)/man1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/$(BIN)
	rm -f $(DESTDIR)$(MANPREFIX)/man1/$(BIN).1

clean:
	rm -f $(BIN) $(OBJ)

.SUFFIXES: .def.h

.def.h.h:
	cp $< $@

.PHONY: all install uninstall clean
