#include <sys/socket.h>

#include <ctype.h>
#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "durinn.h"

extern char *prefix;
extern char *joinprefix;
extern char *partprefix;
extern char *quitprefix;
extern char *tz;

void
eprintf(char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	exit(1);
}

void
wprintf(char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
}

char *
fmttimes(char *s, char *fmt)
{
	char buf[129];
	const char *errstr;
	time_t t;
	struct tm *ttm;

	memset(buf, 0, sizeof(buf));
	t = strtonum(s, 1, LONG_MAX, &errstr);
	if (errstr)
		return NULL;
	if ((ttm = localtime(&t)) == NULL)
		eprintf("localtime\n");
	if (!strftime(buf, sizeof(buf) - 1, fmt, ttm))
		eprintf("strftime\n");

	return strdup(buf);
}

void
join(char *t)
{
	Cmd *cmd;

	cmd = createcmd('j', t);
	cmdexec(cmd);
	target = strdup(lasttarget(t));
	free(cmd);
}

char *
lasttarget(char *s)
{
	char *p;

	if ((p = strrchr(s, ',')) == NULL)
		return s;
	if (strlen(p) > 1)
		return p + 1;
	s[strlen(s) - 1] = '\0';
	if ((p = strrchr(s, ',')) == NULL)
		return s;
	return p + 1;
}

char *
maketime(char *fmt)
{
	char buf[129];
	time_t t;
	struct tm *ttm;

	memset(buf, 0, sizeof(buf));
	if (tz != NULL)
		setenv("TZ", tz, 1);
	t = time(NULL);
	if ((ttm = localtime(&t)) == NULL)
		eprintf("localtime\n");

	if (!strftime(buf, sizeof(buf) - 1, fmt, ttm))
		eprintf("strftime\n");

	return strdup(buf);
}

int
memcrlf(char *s, size_t len)
{
	size_t i;

	for (i = 0; i < len; i++)
		if (s[i] == '\r' && s[i + 1] == '\n')
			return i;
	return -1;
}

void
pong(const char *param)
{
	char buf[MAXMSGLEN];

	memset(buf, 0, sizeof(buf));
	snprintf(buf, strlen(param) + 8, "PONG :%s\r\n", param);
	send(s, buf, strlen(buf), 0);
}

int
strexlm(char *s)
{
	int i;

	for (i = 0; s[i]; i++)
		if (s[i] == '!')
			return i;
	return -1;
}

int
strspace(const char *s)
{
	int i;

	for (i = 0; s[i]; i++)
		if (s[i] == ' ' || s[i] == '\t' || s[i] == '\r' ||
		    s[i] == '\n')
			return i;
	return -1;
}

char *
trim(char *s)
{
	int i;

	for (i = strlen(s); i > 0 && isspace(s[i]); i--)
		s[i] = '\0';
	return s;
}
