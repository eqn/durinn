#include "irc.h"
#include "queue.h"

#define LEN(x) (sizeof(x) / sizeof(*(x)))
#define MAXRCVLEN 8192
#define MAXMSGLEN 512
#define MAXMSGPARAMS 15

typedef struct Cmd Cmd;
typedef struct Channel Channel;
typedef struct Msg Msg;

struct Channel {
	char	*name;
	LIST_ENTRY(Channel) channel_entry;
};
struct Cmd {
	char cmdc;
	union {
		char *target;	/* the target of A, M, N, c, j, m, p and t */
		char *nick;	/* the nick of n and w */
	} g;
	char *args;
};

#define ctarget g.target
#define cnick g.nick
#define cmsg g.msg

struct Msg {
	struct msg_type	type;
	char	*prefix;
	char	*params[MAXMSGPARAMS];
};

LIST_HEAD(channels, Channel) channelhead;

/* channel.c */
void	 add_channel(char *);
void	 remove_channel(char *);

/* cmd.c */
int	 cmdexec(Cmd *);
Cmd	*createcmd(char, char *);

/* durinn.c */
extern int s;
extern char *target;
extern char *argv0;

/* msg.c */
void	 freemsg(Msg *);
char	*msgnick(Msg *);
char	*msgnickhost(Msg *);
Msg	*parsemsg(char *, Msg *);
char	*paramsfrom(char **, int);

/* output.c */
char	*doprefix(Msg *);
char	*formatout(Msg *);
void	 printownaction(char *);
void	 printownline(char *);

/* util.c */
void	 eprintf(char *, ...);
void	 wprintf(char *, ...);
char	*fmttimes(char *, char *);
void	 join(char *);
char	*lasttarget(char *);
char	*maketime(char *);
int	 memcrlf(char *, size_t);
void	 pong(const char *);
int	 strexlm(char *);
int	 strspace(const char *);
char	*trim(char *);

/* strlcat.c */
#undef strlcat
size_t	 strlcat(char *, const char *, size_t);

/* strlcpy.c */
#undef strlcpy
size_t	 strlcpy(char *, const char *, size_t);

/* strtonum.c */
#undef strtonum
long long strtonum(const char *, long long, long long, const char **);
