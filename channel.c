#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "durinn.h"

void
add_channel(char *name)
{
	Channel *c;

	LIST_FOREACH(c, &channelhead, channel_entry)
		if (!strcmp(name, c->name))
			return;

	c = calloc(1, sizeof(*c));
	c->name = strdup(name);

	LIST_INSERT_HEAD(&channelhead, c, channel_entry);
}

void
remove_channel(char *name)
{
	Channel *c, *tmpc;

	LIST_FOREACH_SAFE(c, &channelhead, channel_entry, tmpc)
		if (!strcmp(name, c->name)) {
			LIST_REMOVE(c, channel_entry);
			free(c);
			break;
		}
}
