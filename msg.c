#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "durinn.h"

Msgtype	 typebyraw(char *);
char	**parseparams(char *, char **, size_t);
void	 freeparams(char **, size_t);

Msgtype
typebyraw(char *raw)
{
	struct msg_type msgtypes[] = MSG_TYPES;
	Msgtype id = NONE;
	int i;

	for (i = 0; msgtypes[i].raw; i++) {
		if (!strcmp(msgtypes[i].raw, raw)) {
			id = msgtypes[i].id;
			break;
		}
	}

	return id;
}

void
freemsg(Msg *m)
{
	free(m->prefix);
	free(m->type.raw);
	freeparams(m->params, MAXMSGPARAMS);
	free(m);
}

char *
msgnick(Msg *m)
{
	if (m->prefix == NULL || strexlm(m->prefix) < 0)
		return NULL;
	return strndup(m->prefix, strexlm(m->prefix));
}

char *
msgnickhost(Msg *m)
{
	if (m->prefix == NULL || strchr(m->prefix, '!') == NULL)
		return NULL;
	return strdup(strchr(m->prefix, '!') + 1);
}

Msg *
parsemsg(char *msg, Msg *m)
{
	char command[MAXMSGLEN];

	if (*msg == ':') {
		msg++;
		m->prefix = strndup(msg, strspace(msg));
		msg = strchr(msg, ' ') + 1;
	} else
		m->prefix = NULL;
	strlcpy(command, msg, strspace(msg) + 1);
	m->type.id = typebyraw(command);
	m->type.raw = strdup(command);

	msg = strchr(msg, ' ') + 1;
	parseparams(msg, m->params, MAXMSGPARAMS);

	return m;
}

char **
parseparams(char *params, char *argv[], size_t len)
{
	char **ap;

	for (ap = argv; ap < &argv[len - 1]; ap++) {
		if (*params == ':') {
			*ap = strdup(params + 1);
			break;
		}
		if (strspace(params) < 0) {
			*ap = strdup(params);
			break;
		}
		*ap = strndup(params, strspace(params));
		params += strspace(params) + 1;
	}

	ap++;
	*ap = NULL;

	return argv;
}

void
freeparams(char **params, size_t n)
{
	size_t i;

	for (i = 0; i < n; i++)
		free(params[i]);
}

char *
paramsfrom(char **params, int from)
{
	char buf[MAXMSGLEN];
	int i;

	memset(buf, 0, sizeof(buf));
	if (params[from] == NULL)
		return NULL;
	strlcpy(buf, params[from], sizeof(buf));
	for (i = from + 1; params[i] != NULL; i++) {
		strlcat(buf, " ", sizeof(buf));
		strlcat(buf, params[i], sizeof(buf));
	}

	return strdup(buf);
}
